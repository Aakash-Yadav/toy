from flask import Flask
from flask_sqlalchemy import SQLAlchemy 
from os import urandom,listdir
from flask_login import LoginManager


app = Flask(__name__);

db = SQLAlchemy()
    
app.config["SECRET_KEY"] = "%s"%(urandom(150))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///User.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 0;

db.init_app(app=app)

from .views import views 
from .auth import auth 

app.register_blueprint(views,url_prefix='/')
app.register_blueprint(auth,url_prefix='/')


from .db_models import User_database 

def creat_data_base(app_):
    if "User.db" in listdir('.'):
        pass 
    else:
        db.create_all(app=app_)

creat_data_base(app)

login_manager = LoginManager()
login_manager.login_view = 'auth.login_page';
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return User_database.query.get(int(user_id))






