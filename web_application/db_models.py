from flask_login import UserMixin
from . import db 
from datetime import datetime
from werkzeug.security import generate_password_hash

class User_database(db.Model,UserMixin):
    
    id = db.Column(db.Integer, primary_key=True);
    username = db.Column(db.String(50), unique=True, nullable=False);
    email = db.Column(db.String(80), unique=True, nullable=False)
    password =  db.Column(db.String(150), unique=True, nullable=False)
    date_time = db.Column(db.String(15),default="%s"%(datetime.now().date()));

    __slots__ = ("username_given","email__given","password__given")
    
    def __init__(self,username_given,email_given,password_given) -> None:
        self.username = username_given
        self.email = email_given
        self.password = generate_password_hash(password_given)
