
from flask import Blueprint,render_template


views = Blueprint("views",__name__) 

@views.get('/')
@views.route('/home')
def home_page():
    return render_template("home.html")

@views.get("/about")
def about():
    return "About_page";