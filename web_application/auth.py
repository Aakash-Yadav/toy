from flask import Blueprint,session,render_template,flash,redirect,url_for
from .web_forms import (Register,Login)
from .database_functions import Database_Function
from flask_login import login_user,login_required,current_user 

auth = Blueprint("auth",__name__) 


@auth.route('/login',methods=["GET","POST"])
def login_page():
    form = Login()
    if form.validate_on_submit():
        session['email'] =  form.email.data 
        session['password'] = form.password.data 
        
        exist_or_not = Database_Function(email=session['email'],
            password=session['password']).check_for_login()
        
        if exist_or_not[1]==1:
            login_user(exist_or_not[0])
            flash("login successful")
        else:
            flash(exist_or_not[0])
        
        return redirect(url_for("auth.dashbord"))
    
    return render_template('login.html',xfor=form)


@auth.route('/dashbord')
@login_required
def dashbord():
    k = current_user
    print(k.username)
    print(k.id)
    print(k.email)
    return f"Hola {k.username}"

@auth.route('/logout')
def logout_page():
    return "<h1>Logout page </h1>"


@auth.route('/signup',methods=["GET","POST"])
def signup_page():
    
    form = Register()
    
    if form.validate_on_submit():
        session["name"] = form.username.data;
        session["email"] = form.email.data;
        session['password'] = form.password.data;
        fake_password = form.pass_conform.data 
        
        if(fake_password==session['password']):
            adding_to_db = Database_Function(name=session['name'],
            email=session['email'],
            password=session['password']).adding_user_to_data_base()
            if isinstance(adding_to_db,int):
                flash('your account has been added continue by login')
                return redirect(url_for("auth.login_page"))
            else:
                flash(adding_to_db)
    
    return render_template("registration.html",xfor=form,main_title="Registration")