from . import db 
from .db_models import User_database
from werkzeug.security import check_password_hash 

class Database_Function():
    
    __slots__ = ('name','email',"password")
    
    def __init__(self,name=0,email=0,password=0) -> None:
        self.name = name 
        self.email = email
        self.password = password
    
    def adding_user_to_data_base(self):
        user_exist_or_not = User_database.query.filter_by(
            username=self.name
        ).first()
        print(user_exist_or_not)
        email_exist_or_not =User_database.query.filter_by(
            email=self.email
        ).first()
        print(email_exist_or_not)
        if user_exist_or_not:
            return "username already taken";
        elif email_exist_or_not:
            return "email already registered";
        else:
            new_user = User_database(username_given=self.name,
            email_given=self.email,password_given=self.password)
            db.session.add(new_user);
            db.session.commit()
            return 1;
    
    def delete_user_from_data_base(self):
        delete_user = User_database.query.filter_by(
            username = self.name
        ).first()
        if delete_user:
            db.session.delete(self.delete_user.id)
            db.session.commit()
            return 1
        else:
            return 0
    
    def check_for_login(self):
        data = User_database.query.filter_by(email=self.email).first()
        
        if data:
            if check_password_hash(data.password,self.password):
                print("YESS")
                return (data,1)
            else:
                return ( "Email exist but password is invalid",0)
        else:
            return ("email doesn't exist",0)

    
    def read_all_from_db(self):
        return User_database.query.all()
        